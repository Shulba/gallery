let singlePictureWrapper = 300;
let zoomStatus = false;
let autorunState = true;
let autorunInterval;
$(document).ready(function(){
    difineGalleryObject()
    chosePictures(imageObject.currentImage)
    $(".gallery-btn").on("click", function(){
        autorunState = false;
        changeGallery(this.id)
    })
    $(".single-image ").on("click", function(){
        autorunState = false;
        console.log(this.childNodes[0])
        choseAndViewPictures(this.childNodes[0].src)
        imageObject.currentImage = parseInt(this.id)
        chosePictures(imageObject.currentImage)
    })
    $(".dot-class").on("click", function(){
        autorunState = false;
        imageObject.currentImage = parseInt(this.id)
        chosePictures(imageObject.currentImage)
    })
    if(autorunState){
        autorun()
    }
})
function autorun(){
    autorunInterval= setInterval(function(){
        changeGallery("right")
        if(autorunState==false){
            clearInterval(autorunInterval)
        }
    }, 3000)
}
class SingleImage{
    constructor(id, content){
        this.id = id;
        this.content = content;
    }
}
class AllImageObject{
    constructor(){
        this.array = [];
        this.currentImage = 1;
        this.imageId = 0;
    }
    changeCurrentImages(state){
        if(state){
            this.currentImage += 1
            if(this.currentImage>=this.array.length+1){
                this.currentImage = 1;
            }
        }else if(state == false){
            this.currentImage -= 1
            if(this.currentImage<=0){
                this.currentImage = this.array.length;
            }
        }
    }
    increasePicturesId(){
        return this.imageId += 1;
    }
    addObjectToArr(objectElement){
        let currentGalleryObject = new SingleImage(this.increasePicturesId(), objectElement);
        this.array.push(currentGalleryObject);
    }
}
function changeGallery(idState){
    if(idState=="right"){
        imageObject.changeCurrentImages(true)
        chosePictures(imageObject.currentImage)
    }else if(idState=="left"){
        imageObject.changeCurrentImages(false)
        chosePictures(imageObject.currentImage)
    }
}
let imageObject = new AllImageObject();
function chosePictures(id){
    let obj = $(`#${id}`)   //originalEvent
    let moveObject = -1;
    $("#inner-image-area").stop().animate({
        marginLeft: `${$("#inner-image-area")[0].offsetLeft + (obj[0].offsetLeft*moveObject+$("#image-area")[0].clientWidth/3)}px`
    },500);
    for(let i of $(".dot-class")){
        $(`#${i.id}`).attr("class", "dot-class picture-empty-dot")
    }
    for(let i of $(".single-image")){
        $(`#${i.id}`).attr("class", "single-image opacity-block")
    }
    $(`#${id}-dot`).attr("class", "dot-class picture-dot")
    $(`#${id}`).attr("class", "single-image")
}
function difineGalleryObject(){
    let allBlock = ``;
    $("#image-area").append(`<ul id="dot-selected-area" class="dot-selected-area"></ul>`)
    .append(`<div id="inner-image-area"> ${allBlock} </div>`)
    imageDatabase.picturesGallery.forEach((image) => {
        imageObject.addObjectToArr(`<div class="single-image"><img src='./img/${image.src}'></div>`)
        createDots($("#inner-image-area"), imageObject.imageId, "single-image", true, image, "<div></div>" )
        createDots($("#dot-selected-area"), `${imageObject.imageId}-dot`, "dot-class picture-dot", false, NaN, "<li></li>")
    })
    $("#inner-image-area").css("width", totalInnerGalleryWidth())
}
function totalInnerGalleryWidth(){
    singlePictureWrapper = $(".single-image")[0].clientHeight*2;
    return imageDatabase.picturesGallery.length * singlePictureWrapper
}
function choseAndViewPictures(pictures){
    let galleryBorderObject = $("<div></div>")
    .attr("class", "border-area")
    .attr("id", "display-pictures-area")
    $("#gallery-wrapper").prepend(galleryBorderObject)
    $("#display-pictures-area").html(`<div id="display-window"><div class="close-btn" onclick="closeWindow('display-window')">&#x02A35;</div>
    <div id="right" class="button-right gallery-btn-top">&#10095;</div>
    <div id="left" class="button-left gallery-btn-top">&#10094;</div>
    <div id="gallary-show-wrapper" class="container gallary-show-wrapper"></div></div>`)
    $("#gallary-show-wrapper").html(`<img class="apear-pictures" src="${pictures}">`)
    $("#display-pictures-area").append("<div class='zoom-sign'>&#8981;</div>")
    $(".gallery-btn-top").on("click", function(){
        changeGallery(this.id);
        let nextImage = $(`#${imageObject.currentImage}`)
        $("#gallary-show-wrapper").html(`<img class="apear-pictures" src="${nextImage[0].firstChild.currentSrc}">`);
        largeDescription();
        cancelZoom();
    })
    $(".zoom-sign").on("click", function(){
        if(zoomStatus == false){
            zoomStatus = true;
            $(".apear-pictures").css("transform", "scale(2)");
        }else{
            cancelZoom()
        }
    })
    $("#gallary-show-wrapper").mousemove(function(e){
        if(zoomStatus){
            $(".apear-pictures").css("margin-top", (e.originalEvent.clientY/1.3-350)*-1)
            .css("margin-left", (e.originalEvent.clientX/1.3-550)*-1)
        }
    })
    largeDescription()
}
function cancelZoom(){
    zoomStatus = false;
    $(".apear-pictures").css("transform", "scale(1)")
            .css("margin-top", "0")
            .css("margin-left", "0")
}
function largeDescription(){
    $("#gallary-show-wrapper").prepend(`<div class="descriptionArea-large">
        <h2>${imageDatabase.picturesGallery[imageObject.currentImage-1].heading}</h2>
        <p>${imageDatabase.picturesGallery[imageObject.currentImage-1].description}</p>
        </div>`)
}
function closeWindow(){
    $("#display-pictures-area").remove();
    cancelZoom();
}
function createDots(obj, id, styleClass, state, image, hmlElements){
    let newHTMLObj = $(hmlElements)
    .attr("class", styleClass)
    .attr("id", id);
    obj.append(newHTMLObj)
    if(state){
        $(`#${id}`).html(`<img src='./img/${image.src}'>`);
        $(`#${id}`).append(`<div class="descriptionArea">
        <h2>${image.heading}</h2>
        <p>${image.description}</p>
        </div>`)
    }
}
